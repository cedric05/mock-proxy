const https = require('https');
const http = require('http');
const fs = require('fs');
const proxy =  require('http-proxy')

server = process.argv[2]

var httpproxy = proxy.createProxyServer({
  target:server,
  changeOrigin: true,
});

var localproxy = proxy.createProxyServer({
  target:'http://localhost:8000',
});

const options = {
  key: fs.readFileSync('key.key'),
  cert: fs.readFileSync('crt.crt')
};

https.createServer(options,(req, res) => {
  delete req.headers['accept-encoding']
  if (req.url.startsWith("/static")){
    console.log('goint go static', req.url)
    localproxy.web(req, res)}
  else {
    console.log('goint go original server', req.url)
    httpproxy.web(req,res);
  }
}).listen(8001);


