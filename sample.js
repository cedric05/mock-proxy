http = require('http')
const https = require('https');
fs = require('fs')

proxy = require('http-proxy').createServer({
    target:'https://somewebsite.com',
    secure:false
});
global.a = []

const options = {
    key: fs.readFileSync('key.key'),
    cert: fs.readFileSync('crt.crt')
  };
https.createServer(options,function(req, res){
    delete req.headers['accept-encoding'];
    res.oldwrite = res.write;
    res.oldend = res.end;
    data = "";
    res.write = function(d){
        data+= d;
        res.oldwrite(d);
    }
    res.end = function(){
        a.push([req.url, data]);
        // console.log(a);
        res.oldend();
        data ='';
    }
    
    proxy.web(req,res);
}).listen(8080)
process.on('SIGINT', function(){
    
    fs.writeFileSync("output.json",JSON.stringify(a))
    process.exit();
})
