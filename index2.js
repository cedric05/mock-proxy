const cluster = require('cluster');
if( cluster.isMaster) {
        var cpuCount = require('os').cpus().length/2;
        // Create a worker for each CPU
        for (var i = 0; i < cpuCount; i += 1) {
                cluster.fork();
        }
        cluster.on('exit', function(worker, code, signal) {
        if (worker.exitedAfterDisconnect !== true && (signal || code !== 0)) {
                console.log('Worker died. Restarting a new worker');
                cluster.fork();
                }
        })
}
else {
        console.log('booted up process with pid', process.pid);
        const https = require('https');
        const http = require('http');
        const fs = require('fs');
        const proxy =  require('http-proxy')
        var finalhandler = require('finalhandler');
        var serveStatic = require('serve-static');

        var static = serveStatic(process.argv[3] || "/Users/officeuse/sample/ranga");

        server = process.argv[2] || 'https://qa-pf-alpha2.aviso.com'

        var httpproxy = proxy.createProxyServer({
        target:server,
        changeOrigin: true,
        });

        var localproxy = proxy.createProxyServer({});
        var port = 8000;
        const options = {
        key: fs.readFileSync('key.key'),
        cert: fs.readFileSync('crt.crt')
        };
        vue_urls = ['/static/js/app.js?v=ascent', '/static/js/config.js', '/static/js/admin_landing.js']


        https.createServer(options,(req, res) => {
                req.headers['referer'] = server;
                req.headers['user-agent']= 'Gnana SDK';
                req.rawHeaders['User-Agent'] = 'Gnana SDK';
                if (req.url === '/') {
                        console.log(process.pid, 'redirecting as request coming to /')
                        res.setHeader('location', 'https://localhost:'+ port+ '/welcome/home');
                        res.writeHead(301)
                        res.end()
                }
//                else if (req.url.startsWith('/napi')){
//                        console.log(process.pid, "--napi")
//                        localproxy.web(req, res, {"target": 'http://localhost:3000/'})
//                }
                else if (vue_urls.includes(req.url)){
                        console.log(process.pid, "--> webpack", req.url);
                        localproxy.web(req, res,{"target":"http://localhost:9000/"});
                }
                else if (req.url.startsWith("/static")){
                        console.log(process.pid, '--> static', req.url);
                        // localproxy.web(req, res, {'target': "http://localhost:8000/"})
                        var done = finalhandler(req, res);
                        static(req, res, done);
                }
                else {
                        console.log(process.pid, '--> server', server+req.url)
                        httpproxy.web(req,res);
                }
        }).listen(port);
}
